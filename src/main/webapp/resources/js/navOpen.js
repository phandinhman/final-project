/**
 * Created by Dinh Man on 16/05/2016.
 */
function nav_open() {
    if (getStyleValue(document.getElementById("nav_tutorials"), "display") == "none") {
        document.getElementById("nav_tutorials").style.display = "block";
        return;
    } else if (getStyleValue(document.getElementById("nav_tutorials"), "display") == "block") {
        document.getElementById("nav_tutorials").style.display = "none";
        return;
    }
}
function getStyleValue(elmnt, style) {
    if (window.getComputedStyle) {
        return window.getComputedStyle(elmnt, null).getPropertyValue(style);
    } else {
        return elmnt.currentStyle[style];
    }
}

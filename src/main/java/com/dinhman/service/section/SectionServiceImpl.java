package com.dinhman.service.section;

import com.dinhman.dto.section.SectionDto;
import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.section.SectionEntity;
import com.dinhman.repository.course.CourseRepository;
import com.dinhman.repository.section.SectionRepository;
import com.dinhman.service.lecture.LectureService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinh Man on 29/04/2016.
 */
@Service
public class SectionServiceImpl implements SectionService {

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private LectureService lectureService;

    @Autowired
    private Mapper mapper;

    @Override
    public List<SectionDto> getAllSection(long courseId) {
        List<SectionEntity> sectionEntities = sectionRepository.findByCourseId_id(courseId);
        List<SectionDto> sections = new ArrayList<>();
        for (SectionEntity sectionEntity : sectionEntities) {
            SectionDto section = mapper.map(sectionEntity, SectionDto.class);
            section.setLectures(lectureService.findBySectionId(sectionEntity.getId()));
            sections.add(section);
        }
        return sections;
    }
}

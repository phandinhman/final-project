package com.dinhman.service.section;

import com.dinhman.dto.section.SectionDto;
import com.dinhman.entity.section.SectionEntity;

import java.util.List;

/**
 * Created by Dinh Man on 29/04/2016.
 */
public interface SectionService {

    List<SectionDto> getAllSection(long courseId);

}

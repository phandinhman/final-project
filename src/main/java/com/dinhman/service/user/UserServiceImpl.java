package com.dinhman.service.user;

import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.user.UserEntity;
import com.dinhman.exception.ResourceNotFound;
import com.dinhman.exception.ValidateException;
import com.dinhman.repository.user.UserRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.Mapper;
import org.scribe.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Dinh Man on 18/04/2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Mapper mapper;

    @Override
    public UserDto checkLogin(UserDto user) {

        if (user != null) {
            UserEntity anotherUser = userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());
            if (anotherUser != null) {
                UserDto userDto = mapper.map(anotherUser, UserDto.class);
                return userDto;
            } else {
                throw new ResourceNotFound();
            }
        } else {
            throw new ValidateException("Invalid data.");
        }

    }

    @Override
    public UserDto getUserFromFacebook(Response response) throws IOException {

        String responseBody = response.getBody();

        System.out.println(responseBody);

        JsonNode jsonNode = objectMapper.readTree(responseBody);

        JsonNode idNode = jsonNode.get("id");
        JsonNode nameNode = jsonNode.get("name");

        UserEntity user = userRepository.findByFacebookId(idNode.asText());

        if (user != null) {
            return mapper.map(user, UserDto.class);
        } else {
            UserEntity userEntity = new UserEntity();

            userEntity.setName(nameNode.asText());
            userEntity.setCreateDate(new Date());
            userEntity.setFacebookId(idNode.asText());

            //userEntity.setRoleId(roleEntity);

            UserEntity anotherUser = userRepository.save(userEntity);

            return mapper.map(anotherUser, UserDto.class);

        }

    }

    @Override
    public UserDto getUserFromGoogle(Response response) throws IOException {

        if (response.getCode() == 200) {
            String responseBody = response.getBody();
            JsonNode jsonNode = objectMapper.readTree(responseBody);
            JsonNode idNode = jsonNode.get("id");
            JsonNode emailNode = jsonNode.get("email");
            JsonNode nameNode = jsonNode.get("name");
            JsonNode givenNameNode = jsonNode.get("given_name");
            JsonNode familyNameNode = jsonNode.get("family_name");
            JsonNode linkNode = jsonNode.get("link");
            JsonNode pictureNode = jsonNode.get("picture");
            JsonNode genderNode = jsonNode.get("gender");

            UserEntity user = userRepository.findByEmail(emailNode.asText());

            if (user != null) {
                return mapper.map(user, UserDto.class);
            } else {
                UserEntity userEntity = new UserEntity();
                userEntity.setFirstName(familyNameNode.asText());
                userEntity.setLastName(givenNameNode.asText());
                userEntity.setAvatar(pictureNode.asText());
                userEntity.setName(nameNode.asText());
                userEntity.setEmail(emailNode.asText());
                userEntity.setCreateDate(new Date());
                //userEntity.setRoleId(new RoleEntity(3));
                UserEntity anotherUser = userRepository.save(userEntity);
                return mapper.map(anotherUser, UserDto.class);
            }

        }
        return null;

    }

    @Override
    public UserDto findById(long userId) {
        UserEntity user = userRepository.findOne(userId);
        if (user != null) {
            return mapper.map(user, UserDto.class);
        } else {
            throw new ResourceNotFound("User has " + userId + " not exist.");
        }
    }

    @Override
    public Long numberOfStudent() {
        return userRepository.countByRole_id(new Long(3));
    }
}

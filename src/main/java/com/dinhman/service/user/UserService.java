package com.dinhman.service.user;

import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.user.UserEntity;
import org.scribe.model.Response;

import java.io.IOException;

/**
 * Created by Dinh Man on 18/04/2016.
 */
public interface UserService {

    UserDto checkLogin(UserDto user);

    UserDto getUserFromFacebook(Response response) throws IOException;

    UserDto getUserFromGoogle(Response response) throws IOException;

    UserDto findById(long userId);

    Long numberOfStudent();
}

package com.dinhman.service.discount;

import com.dinhman.entity.discount.DiscountEntity;

/**
 * Created by Dinh Man on 27/04/2016.
 */
public interface DiscountService {

    DiscountEntity findLatestDiscount(long courseID);
}

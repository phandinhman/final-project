package com.dinhman.service.discount;

import com.dinhman.entity.discount.DiscountEntity;
import com.dinhman.repository.discount.DiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dinh Man on 27/04/2016.
 */
@Service
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private DiscountRepository discountRepository;

    @Override
    public DiscountEntity findLatestDiscount(long courseID) {
        return discountRepository.findDiscountLatest(courseID);
    }
}

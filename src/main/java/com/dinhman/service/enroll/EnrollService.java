package com.dinhman.service.enroll;

import com.dinhman.dto.course.CourseDto;
import com.dinhman.entity.enroll.EnrollEntity;

import java.util.List;

/**
 * Created by Dinh Man on 16/05/2016.
 */
public interface EnrollService {

    List<CourseDto> enrolled(long userId);

    EnrollEntity findEnrollByUserIdAndCourseId(long userId, long courseId);

    EnrollEntity save(long userId, long courseId);

}

package com.dinhman.service.enroll;

import com.dinhman.dto.course.CourseDto;
import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.user.UserEntity;
import com.dinhman.repository.enroll.EnrollRepository;
import com.dinhman.service.user.UserService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 16/05/2016.
 */
@Service
public class EnrollServiceImpl implements EnrollService {

    @Autowired
    private EnrollRepository enrollRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private Mapper mapper;

    @Override
    public List<CourseDto> enrolled(long userId) {

        UserDto userDto = userService.findById(userId);

        List<EnrollEntity> coursesEntity = enrollRepository.findByUserId_id(userDto.getId());

        List<CourseDto> courses = new ArrayList<>();

        for (EnrollEntity courseEntity : coursesEntity) {
            CourseEntity course = courseEntity.getCourseId();
            CourseDto courseDto = mapper.map(course, CourseDto.class)
                    .setUser(mapper.map(course.getUserId(), UserDto.class));
            courses.add(courseDto);
        }

        return courses;

    }

    @Override
    public EnrollEntity findEnrollByUserIdAndCourseId(long userId, long courseId) {
        EnrollEntity enroll = enrollRepository.findByUserId_idAndCourseId_id(userId, courseId);
        return enroll;
    }

    @Override
    public EnrollEntity save(long userId, long courseId) {
        EnrollEntity enroll = new EnrollEntity();
        enroll.setCourseId(new CourseEntity(courseId));
        enroll.setUserId(new UserEntity(userId));
        enroll.setEnrollDate(new Date());
        return enrollRepository.save(enroll);
    }
}

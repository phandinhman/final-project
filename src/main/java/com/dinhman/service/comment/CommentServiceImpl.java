package com.dinhman.service.comment;

import com.dinhman.constant.Constants;
import com.dinhman.dto.comment.CommentDto;
import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.comment.CommentEntity;
import com.dinhman.repository.comment.CommentRepository;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinh Man on 29/05/2016.
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private Mapper mapper;

    @Override
    public List<CommentDto> findComment(Long courseId, int pageNumber) {

        PageRequest pageRequest = new PageRequest(pageNumber - 1, Constants.PAGE_SIZE);
        Page<CommentEntity> page = commentRepository.findAll(pageRequest);

        List<CommentDto> comments = new ArrayList<>();


        for (CommentEntity commentEntity : page.getContent()) {
            CommentDto comment = mapper.map(commentEntity, CommentDto.class)
                    .setUser(mapper.map(commentEntity.getUserId(), UserDto.class));
            comments.add(comment);
        }

        return comments;
    }
}

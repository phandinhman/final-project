package com.dinhman.service.comment;

import com.dinhman.dto.comment.CommentDto;

import java.util.List;

/**
 * Created by Dinh Man on 29/05/2016.
 */
public interface CommentService {
    List<CommentDto> findComment(Long courseId, int page);
}

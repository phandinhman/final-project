package com.dinhman.service.content;

import com.dinhman.dto.content.ContentDto;
import com.dinhman.entity.content.ContentEntity;
import com.dinhman.repository.content.ContentRepository;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinh Man on 25/05/2016.
 */
@Service
public class ContentServiceImpl implements ContentService {

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private Mapper mapper;

    @Override
    public List<ContentDto> findByLectureId(Long lectureId) {
        List<ContentDto> contents = new ArrayList<>();
        List<ContentEntity> contentEntities = contentRepository.findByLecture_id(lectureId);

        for (ContentEntity content : contentEntities) {
            contents.add(mapper.map(content, ContentDto.class));
        }
        return contents;
    }
}

package com.dinhman.service.content;

import com.dinhman.dto.content.ContentDto;

import java.util.List;

/**
 * Created by Dinh Man on 25/05/2016.
 */
public interface ContentService {

    List<ContentDto> findByLectureId(Long lectureId);

}

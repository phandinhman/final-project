package com.dinhman.service.course;

import com.dinhman.constant.Constants;
import com.dinhman.dto.course.CourseDto;
import com.dinhman.dto.discount.DiscountDto;
import com.dinhman.dto.type.TypeDto;
import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.discount.DiscountEntity;
import com.dinhman.exception.ResourceNotFound;
import com.dinhman.repository.course.CourseRepository;
import com.dinhman.repository.enroll.EnrollRepository;
import com.dinhman.repository.lecture.LectureRepository;
import com.dinhman.repository.rate.RateRepository;
import com.dinhman.service.discount.DiscountService;
import com.dinhman.service.enroll.EnrollService;
import com.dinhman.service.section.SectionService;
import com.dinhman.service.user.UserService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinh Man on 18/03/2016.
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private DiscountService discountService;

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private LectureRepository lectureRepository;

    @Autowired
    private EnrollRepository enrollRepository;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private Mapper mapper;

    @Autowired
    private UserService userService;

    @Autowired
    private EnrollService enrollService;

    @Override
    public List<CourseDto> findAllCourse() {
        List<CourseDto> courses = new ArrayList<>();
        List<CourseEntity> courseEntities = courseRepository.findAll();

        for (CourseEntity courseEntity : courseEntities) {
            CourseDto course = mapper.map(courseEntity, CourseDto.class);
            course.setNumberOfEnrolled(courseEntity.getEnrollEntityList().size());
            course.setUser(mapper.map(courseEntity.getUserId(), UserDto.class));
            courses.add(course);
        }

        return courses;
    }

    @Override
    public List<CourseDto> getALLCourse() {
        return findAllCourse();
    }

    @Override
    public CourseDto findById(long idCourse) {
        CourseEntity courseEntity = courseRepository.findOne(idCourse);
        if (courseEntity != null) {
            long numberOfLecture = lectureRepository.numberOfLectureOfCourse(idCourse);
            CourseDto course = mapper.map(courseEntity, CourseDto.class).setNumberOfLecture(numberOfLecture);
            course.setType(mapper.map(courseEntity.getTypeId(), TypeDto.class));
            course.setUser(mapper.map(courseEntity.getUserId(), UserDto.class))
                    .setNumberOfEnrolled(enrollRepository.getNumberOfEnrolled(idCourse))
                    .setLengthOfCourse(lectureRepository.timeOfCourse(idCourse))
                    .setSections(sectionService.getAllSection(idCourse));

            DiscountEntity latestDiscount = discountService.findLatestDiscount(course.getId());
            Double averageRate = rateRepository.averageRate(course.getId());
            if (latestDiscount != null) {
                course.setDiscount(mapper.map(latestDiscount, DiscountDto.class));
            }
            if (averageRate != null) {
                course.setAverageRate(averageRate);
            }

            return course;
        } else {
            throw new ResourceNotFound("Course not found.");
        }
    }

    @Override
    public CourseDto findById(long idCourse, long enrollId) {
        CourseDto course = findById(idCourse);
        course.setNumberOfLectureLearned(enrollRepository.numberOfLectureLearned(enrollId));
        return course;
    }

    @Override
    public List<CourseDto> findByName(String name) {

        List<CourseDto> courses = new ArrayList<>();
        List<CourseEntity> courseEntities = courseRepository.findByNameLike(name);

        for (CourseEntity courseEntity : courseEntities) {
            courses.add(mappingCourse(courseEntity));
        }

        return courses;

    }

    /*@Override
    public CourseDto update(CourseDto course) throws ResourceNotFound {
        CourseEntity courseEntity = mapper.map(course, CourseEntity.class);
        if (courseEntity != null) {
            return courseRepository.save(anotherCourse);
        } else {
            throw new ResourceNotFound("Course not found.");
        }

    }*/

    @Override
    public CourseDto delete(long id) throws ResourceNotFound {
        CourseEntity courseEntity = courseRepository.findOne(id);
        if (courseEntity != null) {
            courseRepository.delete(id);
            return mapper.map(courseEntity, CourseDto.class);
        } else {
            throw new ResourceNotFound("Course not found");
        }
    }

    @Override
    public List<CourseDto> topCourse() {
        List<CourseEntity> topCourse = courseRepository.findTopCourse();
        List<CourseDto> courses = new ArrayList<>();

        for (CourseEntity courseEntity : topCourse) {
            courses.add(mappingCourse(courseEntity));
        }
        return courses;
    }

    @Override
    public List<CourseDto> topLatest() {
        List<CourseEntity> topLatestCourse = courseRepository.findTop4ByOrderByCreateDateDesc();
        List<CourseDto> courses = new ArrayList<>();

        for (CourseEntity courseEntity : topLatestCourse) {
            courses.add(mappingCourse(courseEntity));
        }

        return courses;
    }

    @Override
    public List<CourseDto> sameType(long typeId) {
        List<CourseEntity> sameType = courseRepository.findTop3ByTypeId_id(typeId);
        List<CourseDto> courses = new ArrayList<>();

        for (CourseEntity courseEntity : sameType) {
            courses.add(mappingCourse(courseEntity));
        }
        return courses;
    }

    @Override
    public List<CourseDto> enrolled(long userId) {
        return enrollService.enrolled(userId);
    }

    @Override
    public List<CourseDto> getByPage(int pageNumber) {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, Constants.PAGE_SIZE);
        Page<CourseEntity> page = courseRepository.findAll(pageRequest);
        List<CourseDto> courses = new ArrayList<>();

        for (CourseEntity courseEntity : page.getContent()) {
            CourseDto course = mapper.map(courseEntity, CourseDto.class).setUser(mapper.map(courseEntity.getUserId(), UserDto.class)).
                    setNumberOfLecture(enrollRepository.getNumberOfEnrolled(courseEntity.getId()));
            DiscountEntity latestDiscount = discountService.findLatestDiscount(courseEntity.getId());
            Double averageRate = rateRepository.averageRate(courseEntity.getId());
            if (latestDiscount != null) {
                course.setDiscount(mapper.map(latestDiscount, DiscountDto.class));
            }
            if (averageRate != null) {
                course.setAverageRate(averageRate);
            }
            courses.add(course);
        }
        return courses;
    }

    @Override
    public Long countCourse() {
        return courseRepository.countByDeleteAtIsNull();
    }

    private CourseDto mappingCourse(CourseEntity e) {
        CourseDto course = mapper.map(e, CourseDto.class).setNumberOfLecture(e.getEnrollEntityList().size());
        course.setUser(mapper.map(e.getUserId(), UserDto.class)).
                setNumberOfEnrolled(enrollRepository.getNumberOfEnrolled(e.getId()));
        DiscountEntity latestDiscount = discountService.findLatestDiscount(e.getId());
        Double averageRate = rateRepository.averageRate(e.getId());
        if (latestDiscount != null) {
            course.setDiscount(mapper.map(latestDiscount, DiscountDto.class));
        }
        if (averageRate != null) {
            course.setAverageRate(averageRate);
        }
        return course;

    }

}

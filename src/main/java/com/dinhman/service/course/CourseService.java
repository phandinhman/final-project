package com.dinhman.service.course;

import com.dinhman.dto.course.CourseDto;
import com.dinhman.exception.ResourceNotFound;

import java.util.List;

/**
 * Created by Dinh Man on 18/03/2016.
 */
public interface CourseService {
    /**
     * Get all course from database.
     *
     * @return list course.
     */

    List<CourseDto> findAllCourse();

    List<CourseDto> getALLCourse();

    /**
     * Get one course by id.
     *
     * @param idCourse id of course.
     * @return course.
     */
    CourseDto findById(long idCourse);

    /**
     * Get one course by id.
     *
     * @param idCourse id of course.
     * @param enrollId id of enroll.
     * @return course.
     */
    CourseDto findById(long idCourse, long enrollId);


    /**
     * Get one course by id.
     *
     * @param name name of course.
     * @return course.
     */
    List<CourseDto> findByName(String name);

    /**
     * Update one course.
     *
     * @param courseEntity course need edit.
     * @return course updated.
     */
    // CourseEntity update(CourseEntity courseEntity) throws ResourceNotFound;


    /**
     * Delete course.
     *
     * @param id id of course.
     * @return course deleted.
     */
    CourseDto delete(long id) throws ResourceNotFound;

    List<CourseDto> topCourse();

    List<CourseDto> topLatest();

    List<CourseDto> sameType(long typeId);

    List<CourseDto> enrolled(long id);

    List<CourseDto> getByPage(int page);

    Long countCourse();

}

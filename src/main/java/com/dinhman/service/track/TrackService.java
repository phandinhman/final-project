package com.dinhman.service.track;

import com.dinhman.dto.track.TrackDto;
import com.dinhman.repository.track.TrackRepository;

/**
 * Created by Dinh Man on 22/05/2016.
 */
public interface TrackService {

    TrackDto create(TrackDto track);

}

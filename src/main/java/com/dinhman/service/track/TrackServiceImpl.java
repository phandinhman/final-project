package com.dinhman.service.track;

import com.dinhman.dto.track.TrackDto;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.lecture.LectureEntity;
import com.dinhman.entity.track.TrackEntity;
import com.dinhman.exception.ValidateException;
import com.dinhman.repository.track.TrackRepository;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by Dinh Man on 22/05/2016.
 */
@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private Mapper mapper;

    @Override
    public TrackDto create(TrackDto track) {

        if (track != null) {
            TrackEntity entity = new TrackEntity();
            entity.setCreateDate(new Date());
            entity.setLectureId(new LectureEntity(track.getLectureId()));
            entity.setEnrollId(new EnrollEntity(track.getEnrollId()));
            TrackEntity another = trackRepository.save(entity);
            TrackDto trackDto = new TrackDto();
            trackDto.setId(another.getId());
            trackDto.setLectureId(another.getLectureId().getId());
            trackDto.setEnrollId(another.getEnrollId().getId());
            return trackDto;
        } else {
            throw new ValidateException("");
        }
    }
}

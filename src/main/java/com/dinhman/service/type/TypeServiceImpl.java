package com.dinhman.service.type;

import com.dinhman.dto.type.TypeDto;
import com.dinhman.entity.type.TypeEntity;
import com.dinhman.repository.type.TypeRepository;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinh Man on 07/05/2016.
 */
@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeRepository typeRepository;

    @Autowired
    private Mapper mapper;

    @Override
    public List<TypeDto> getAllType() {
        List<TypeEntity> listType = typeRepository.findAll();

        List<TypeDto> listTypeDto = new ArrayList<>();

        for (TypeEntity typeEntity : listType) {
            listTypeDto.add(mapper.map(typeEntity, TypeDto.class));
        }
        return listTypeDto;
    }
}

package com.dinhman.service.type;

import com.dinhman.dto.type.TypeDto;
import com.dinhman.entity.type.TypeEntity;

import java.util.List;

/**
 * Created by Dinh Man on 07/05/2016.
 */
public interface TypeService {
    List<TypeDto> getAllType();
}

package com.dinhman.service.lecture;

import com.dinhman.dto.lecture.LectureDto;

import java.util.List;

/**
 * Created by Dinh Man on 21/05/2016.
 */
public interface LectureService {

    List<LectureDto> findBySectionId(Long sectionId);

    LectureDto findById(Long lectureId);

    LectureDto nextLecture(long courseId, long enrollId);

    Long numberOfLecture();
}

package com.dinhman.service.lecture;

import com.dinhman.dto.lecture.LectureDto;
import com.dinhman.entity.lecture.LectureEntity;
import com.dinhman.exception.ResourceNotFound;
import com.dinhman.repository.lecture.LectureRepository;
import com.dinhman.service.content.ContentService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinh Man on 21/05/2016.
 */
@Service
public class LectureServiceImpl implements LectureService {

    @Autowired
    private LectureRepository lectureRepository;

    @Autowired
    private ContentService contentService;

    @Autowired
    private Mapper mapper;

    @Override
    public List<LectureDto> findBySectionId(Long sectionId) {

        List<LectureEntity> lectureEntities = lectureRepository.findBySection_id(sectionId);
        List<LectureDto> lectures = new ArrayList<>();

        for (LectureEntity lectureEntity : lectureEntities) {
            lectures.add(mapper.map(lectureEntity, LectureDto.class));
        }

        return lectures;
    }

    @Override
    public LectureDto findById(Long lectureId) {
        LectureEntity lectureEntity = lectureRepository.findOne(lectureId);
        if (lectureEntity != null) {
            LectureDto lecture = mapper.map(lectureEntity, LectureDto.class);
            if (lecture.getVideo() == null) {
                lecture.setContents(contentService.findByLectureId(lectureId));
            }
            return lecture;
        } else {
            throw new ResourceNotFound("");
        }
    }

    @Override
    public LectureDto nextLecture(long courseId, long enrollId) {
        LectureEntity lectureEntity = lectureRepository.nextLecture(courseId, enrollId);
        if (lectureEntity != null) {
            return mapper.map(lectureEntity, LectureDto.class);
        }
        return null;
    }

    @Override
    public Long numberOfLecture() {
        return lectureRepository.countWhereDeleteAtIsNull();
    }
}

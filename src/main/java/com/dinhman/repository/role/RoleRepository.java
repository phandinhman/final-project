package com.dinhman.repository.role;

import com.dinhman.entity.role.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by framgia on 13/04/2017.
 */
public interface RoleRepository extends JpaRepository<RoleEntity, Integer>{
    RoleEntity findByName(String name);
}

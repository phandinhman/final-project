package com.dinhman.repository.enroll;

import com.dinhman.entity.enroll.EnrollEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface EnrollRepository extends JpaRepository<EnrollEntity, Long> {
    List<EnrollEntity> findByUserId_id(long id);

    @Query(value = "SELECT COUNT(course_id) FROM ENROLL WHERE COURSE_ID=?", nativeQuery = true)
    Long getNumberOfEnrolled(long courseId);

    EnrollEntity findByUserId_idAndCourseId_id(long userId, long courseId);

    @Query(value = "SELECT count(distinct (lecture_id)) from track inner join enroll " +
            "on enroll.id = track.enroll_id where track.enroll_id = ?", nativeQuery = true)
    long numberOfLectureLearned(long enrollId);

}

package com.dinhman.repository.book;

import com.dinhman.entity.book.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 16/04/2016.
 */
@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {
}

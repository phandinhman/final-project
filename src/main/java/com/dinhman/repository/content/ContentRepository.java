package com.dinhman.repository.content;

import com.dinhman.entity.content.ContentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Dinh Man on 25/05/2016.
 */
public interface ContentRepository extends JpaRepository<ContentEntity, Long> {

    List<ContentEntity> findByLecture_id(long lectureId);

}

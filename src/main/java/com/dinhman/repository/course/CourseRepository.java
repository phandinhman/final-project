package com.dinhman.repository.course;

import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.type.TypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dinh Man on 18/03/2016.
 */
@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, Long> {

    @Query(value = "SELECT *, COUNT(enroll.course_id) AS NumberOfOrders FROM course inner join enroll on course.id = " +
            "enroll.course_id group by enroll.course_id order by NumberOfOrders desc limit 4", nativeQuery = true)
    List<CourseEntity> findTopCourse();

    List<CourseEntity> findTop4ByOrderByCreateDateDesc();

    List<CourseEntity> findTop3ByTypeId_id(long typeId);

    @Query("Select c from CourseEntity c where c.name like %:name%")
    List<CourseEntity> findByNameLike(@Param("name") String name);

    Long countByDeleteAtIsNull();

}

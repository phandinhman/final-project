package com.dinhman.repository.comment;

import com.dinhman.entity.comment.CommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, Long> {

    List<CommentEntity> findByCourse_id(Long courseId);

}

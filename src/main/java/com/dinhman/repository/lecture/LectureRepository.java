package com.dinhman.repository.lecture;

import com.dinhman.entity.lecture.LectureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface LectureRepository extends JpaRepository<LectureEntity, Long> {

    @Query(value = "SELECT count(lecture.id) FROM (lecture inner join section on section_id = section.id) " +
            "inner join course on course.id = section.course_id where course.id = ?;", nativeQuery = true)
    long numberOfLectureOfCourse(long courseId);

    @Query(value = "SELECT sum(duration) FROM (course INNER JOIN section on course.id = section.course_id) " +
            "INNER JOIN lecture ON section.id = lecture.section_id WHERE course.id = ?;", nativeQuery = true)
    long timeOfCourse(long courseId);

    List<LectureEntity> findBySection_id(Long sectionId);

    @Query(value = "SELECT l.id, l.name, l.description, l.video, l.source_code, l.duration, " +
            "l.create_date, l.update_date, l.delete_at, l.section_id " +
            "FROM course c INNER JOIN section s ON c.id = s.course_id " +
            "INNER JOIN lecture l ON l.section_id = s.id " +
            "WHERE c.id = ?1 AND l.id NOT IN (SELECT l.id FROM course c " +
            "INNER JOIN section s ON c.id = s.course_id " +
            "INNER JOIN lecture l ON l.section_id = s.id " +
            "INNER JOIN track t ON l.id = t.lecture_id " +
            "INNER JOIN enroll e ON e.course_id = c.id " +
            "WHERE t.enroll_id = ?2) LIMIT 1", nativeQuery = true)
    LectureEntity nextLecture(long courseId, long enrollId);

    @Query(value = "select count(*) from lecture where delete_at is null", nativeQuery = true)
    Long countWhereDeleteAtIsNull();

}

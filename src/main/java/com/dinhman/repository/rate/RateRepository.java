package com.dinhman.repository.rate;

import com.dinhman.entity.rate.RateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface RateRepository extends JpaRepository<RateEntity, Long> {

    @Query(value = "SELECT avg(vote_id) from rate inner join enroll on rate.id = enroll.id where enroll.course_id =?", nativeQuery = true)
    Double averageRate(long courseId);
}

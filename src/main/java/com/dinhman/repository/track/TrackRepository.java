package com.dinhman.repository.track;

import com.dinhman.entity.track.TrackEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by Dinh Man on 20/05/2016.
 */
public interface TrackRepository extends JpaRepository<TrackEntity, Long> {

}

package com.dinhman.repository.type;

import com.dinhman.entity.type.TypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 07/05/2016.
 */
@Repository
public interface TypeRepository extends JpaRepository<TypeEntity, Long> {
}

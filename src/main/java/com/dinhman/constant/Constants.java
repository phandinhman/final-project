package com.dinhman.constant;

/**
 * Created by Dinh Man on 23/04/2016.
 */
public final class Constants {

    public static final String FACEBOOK_CLIENT_ID = "1318567448205722";

    public static final String FACEBOOK_CLIENT_SECRET = "4d98ca224cbd2988a734993c2921530b";

    public static final String FACEBOOK_HOST = "http://localhost:8080";

    public static final String FACEBOOK_URL = "https://graph.facebook.com/me";

    public static final String FACEBOOK_STATE = "state";

    public static final String GOOGLE_PROTECTED_RESOURCE_URL = "https://www.googleapis.com/oauth2/v2/userinfo?alt=json";

    public static final String GOOGLE_SCOPE = "https://mail.google.com/ https://www.googleapis.com/auth/userinfo.email";

    public static final String GOOGLE_CLIENT_ID = "405654165743-78cfdrbg2t0ns4t4djgis02aeeg4qjvj.apps.googleusercontent.com";

    public static final String GOOGLE_CLIENT_SECRET = "BI3trUE9HTkE2Ac2aslrTSNw";

    public static final String GOOGLE_REDIRECT_URL = "http://127.0.0.1:8080/auth/google/callback";

    public static final String AUTHORIZE_URL = "https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=%s&redirect_uri=%s";

    public static final String SCOPED_AUTHORIZE_URL = AUTHORIZE_URL + "&scope=%s";

    public static final int PAGE_SIZE = 12;

}

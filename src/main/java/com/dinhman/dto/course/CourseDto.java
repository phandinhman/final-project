package com.dinhman.dto.course;

import com.dinhman.dto.discount.DiscountDto;
import com.dinhman.dto.goal.GoalDto;
import com.dinhman.dto.section.SectionDto;
import com.dinhman.dto.type.TypeDto;
import com.dinhman.dto.user.UserDto;

import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 14/05/2016.
 */
public class CourseDto {

    private long id;

    private String name;

    private long cost;

    private String image;

    private String description;

    private UserDto user;

    private List<GoalDto> goals;

    private TypeDto type;

    private long numberOfLecture;

    private double averageRate;

    private long numberOfEnrolled;

    private long lengthOfCourse;

    private long numberOfLectureLearned;

    private DiscountDto discount;

    private List<SectionDto> sections;

    public CourseDto() {
    }

    public long getId() {
        return id;
    }

    public CourseDto setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CourseDto setName(String name) {
        this.name = name;
        return this;
    }

    public long getCost() {
        return cost;
    }

    public CourseDto setCost(long cost) {
        this.cost = cost;
        return this;
    }

    public String getImage() {
        return image;
    }

    public CourseDto setImage(String image) {
        this.image = image;
        return this;
    }

    public UserDto getUser() {
        return user;
    }

    public CourseDto setUser(UserDto user) {
        this.user = user;
        return this;
    }

    public List<GoalDto> getGoals() {
        return goals;
    }

    public CourseDto setGoals(List<GoalDto> goals) {
        this.goals = goals;
        return this;
    }

    public long getNumberOfLecture() {
        return numberOfLecture;
    }

    public CourseDto setNumberOfLecture(long numberOfLecture) {
        this.numberOfLecture = numberOfLecture;
        return this;
    }

    public double getAverageRate() {
        return averageRate;
    }

    public CourseDto setAverageRate(double averageRate) {
        this.averageRate = averageRate;
        return this;
    }

    public TypeDto getType() {
        return type;
    }

    public CourseDto setType(TypeDto type) {
        this.type = type;
        return this;
    }

    public List<SectionDto> getSections() {
        return sections;
    }

    public CourseDto setSections(List<SectionDto> sections) {
        this.sections = sections;
        return this;
    }

    public long getNumberOfEnrolled() {
        return numberOfEnrolled;
    }


    public DiscountDto getDiscount() {
        return discount;
    }

    public void setDiscount(DiscountDto discount) {
        this.discount = discount;
    }

    public CourseDto setNumberOfEnrolled(long numberOfEnrolled) {
        this.numberOfEnrolled = numberOfEnrolled;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CourseDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public long getLengthOfCourse() {
        return lengthOfCourse;
    }

    public CourseDto setLengthOfCourse(long lengthOfCourse) {
        this.lengthOfCourse = lengthOfCourse;
        return this;
    }

    public long getNumberOfLectureLearned() {
        return numberOfLectureLearned;
    }

    public CourseDto setNumberOfLectureLearned(long numberOfLectureLearned) {
        this.numberOfLectureLearned = numberOfLectureLearned;
        return this;
    }
}

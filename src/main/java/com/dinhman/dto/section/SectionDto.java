package com.dinhman.dto.section;

import com.dinhman.dto.lecture.LectureDto;

import java.util.List;

/**
 * Created by Dinh Man on 21/05/2016.
 */
public class SectionDto {

    private long id;
    private String name;
    private String description;
    private List<LectureDto> lectures;

    public SectionDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LectureDto> getLectures() {
        return lectures;
    }

    public void setLectures(List<LectureDto> lectures) {
        this.lectures = lectures;
    }
}

package com.dinhman.dto.track;

/**
 * Created by Dinh Man on 22/05/2016.
 */
public class TrackDto {

    private long id;
    private long enrollId;
    private long lectureId;

    public TrackDto() {
    }

    public TrackDto(long id, long enrollId, long lectureId) {
        this.id = id;
        this.enrollId = enrollId;
        this.lectureId = lectureId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getEnrollId() {
        return enrollId;
    }

    public void setEnrollId(long enrollId) {
        this.enrollId = enrollId;
    }

    public long getLectureId() {
        return lectureId;
    }

    public void setLectureId(long lectureId) {
        this.lectureId = lectureId;
    }
}

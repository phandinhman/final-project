package com.dinhman.dto.comment;

import com.dinhman.dto.user.UserDto;

import java.util.Date;

/**
 * Created by Dinh Man on 14/05/2016.
 */
public class CommentDto {
    private long id;
    private String content;
    private Date createDate;
    private UserDto user;

    public CommentDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public UserDto getUser() {
        return user;
    }

    public CommentDto setUser(UserDto user) {
        this.user = user;
        return this;
    }
}

package com.dinhman.dto.content;

/**
 * Created by Dinh Man on 25/05/2016.
 */
public class ContentDto {

    private String content;

    private String image;

    public ContentDto() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

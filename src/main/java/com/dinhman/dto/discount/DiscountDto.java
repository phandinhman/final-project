package com.dinhman.dto.discount;

/**
 * Created by Dinh Man on 14/05/2016.
 */
public class DiscountDto {

    private long id;
    private String description;
    private double percent;

    public DiscountDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "DiscountDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", percent=" + percent +
                '}';
    }
}

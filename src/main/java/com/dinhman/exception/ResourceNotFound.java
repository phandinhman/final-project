package com.dinhman.exception;

/**
 * Created by Dinh Man on 06/03/2016.
 */
public class ResourceNotFound extends RuntimeException {
    public ResourceNotFound() {
    }

    public ResourceNotFound(String message) {
        super(message);
    }
}

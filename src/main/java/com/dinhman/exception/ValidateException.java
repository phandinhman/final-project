package com.dinhman.exception;

/**
 * Created by Dinh Man on 18/04/2016.
 */
public class ValidateException extends RuntimeException {

    public ValidateException() {
        super();
    }

    public ValidateException(String errors) {
        super(errors);
    }

}

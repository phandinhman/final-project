package com.dinhman.itext;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Dinh Man on 26/05/2016.
 */
public class JavaPdfHelloWorld {

    public static void main(String[] args) throws DocumentException, IOException {
        System.out.println("Hello World");

        Document document = new Document();

        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("hello.pdf"));

        document.open();
        document.add(new Paragraph("Demo Itext"));
        document.add(new Paragraph("TAND TP HCM vừa mở phiên phúc xử việc ông Lê Đa Ni Ên (con trai ông Lê Ân với người vợ đầu Lê Ngọc Lan) kiện UBND quận Tân Bình ra quyết định hủy bỏ giấy chứng nhận quyền sử dụng căn nhà 408 đường Cách Mạng Tháng 8, quận Tân Bình, đã cấp cho bà Lan. Đây là căn nhà đại gia Lê Ân đang kiện đòi vợ cũ sau hơn 30 năm ly hôn."));

        Image image = Image.getInstance("E:\\InteljJ\\final-project\\src\\main\\webapp\\resources\\image\\course\\80940_2664_8.jpg");

        image.scaleAbsolute(600, 400);


        document.add(image);

        document.close();
        pdfWriter.close();

    }

}

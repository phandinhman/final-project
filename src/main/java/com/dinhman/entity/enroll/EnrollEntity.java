package com.dinhman.entity.enroll;

import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.rate.RateEntity;
import com.dinhman.entity.track.TrackEntity;
import com.dinhman.entity.user.UserEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 17/04/2016.
 */

@Entity
@Table(name = "enroll")
public class EnrollEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private long id;

    @Column(name = "enroll_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enrollDate;

    @JoinColumn(name = "course_id", referencedColumnName = "id")
    @ManyToOne
    private CourseEntity courseId;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private UserEntity userId;

    @OneToOne(mappedBy = "enrollId", fetch = FetchType.EAGER)
    private RateEntity rateId;

    @OneToMany(mappedBy = "enrollId")
    private List<TrackEntity> trackEntityList;

    public EnrollEntity() {
    }

    public EnrollEntity(Long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(Date enrollDate) {
        this.enrollDate = enrollDate;
    }

    public CourseEntity getCourseId() {
        return courseId;
    }

    public void setCourseId(CourseEntity courseId) {
        this.courseId = courseId;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RateEntity getRateId() {
        return rateId;
    }

    public void setRateId(RateEntity rateId) {
        this.rateId = rateId;
    }

    public List<TrackEntity> getTrackEntityList() {
        return trackEntityList;
    }

    public void setTrackEntityList(List<TrackEntity> trackEntityList) {
        this.trackEntityList = trackEntityList;
    }

    @Override
    public String toString() {
        return "EnrollEntity{" +
                "id=" + id +
                ", enrollDate=" + enrollDate +
                ", courseId=" + courseId +
                ", userId=" + userId +
                '}';
    }
}


package com.dinhman.entity.answer;

import com.dinhman.entity.key.KeyEntity;
import com.dinhman.entity.quiz.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 24/04/2016.
 */
@Entity
@Table(name = "answer")
public class AnswerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String answer;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "delete_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteDate;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @JoinColumn(name = "quiz_id", referencedColumnName = "id")
    @ManyToOne
    private QuizEntity quizId;

    @OneToMany(mappedBy = "answerId")
    private List<KeyEntity> listKey;

    public AnswerEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public QuizEntity getQuizId() {
        return quizId;
    }

    public void setQuizId(QuizEntity quizId) {
        this.quizId = quizId;
    }

    public List<KeyEntity> getListKey() {
        return listKey;
    }

    public void setListKey(List<KeyEntity> listKey) {
        this.listKey = listKey;
    }

}

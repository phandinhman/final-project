package com.dinhman.entity.course;


import com.dinhman.entity.comment.CommentEntity;
import com.dinhman.entity.discount.DiscountEntity;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.section.SectionEntity;
import com.dinhman.entity.type.TypeEntity;
import com.dinhman.entity.user.UserEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 06/04/2016.
 */
@Entity
@Table(name = "course")
public class CourseEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private long id;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "delete_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteAt;

    private long cost;

    private String image;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private UserEntity userId;

    @JoinColumn(name = "type_id", referencedColumnName = "id")
    @ManyToOne
    private TypeEntity typeId;

    @OneToMany(mappedBy = "courseEntity")
    private List<DiscountEntity> discountEntity;

    @OneToMany(mappedBy = "courseId")
    private List<SectionEntity> sectionEntityList;

    @OneToMany(mappedBy = "courseId", fetch = FetchType.EAGER)
    private List<EnrollEntity> enrollEntityList;

    //@OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    //private List<GoalEntity> goals;

    @OneToMany(mappedBy = "course")
    private List<CommentEntity> comments;

    public CourseEntity() {
    }

    public CourseEntity(long id) {
        this.id = id;
    }

    public CourseEntity(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    @XmlTransient
    public List<DiscountEntity> getDiscountEntity() {
        return discountEntity;
    }

    public void setDiscountEntity(List<DiscountEntity> discountEntity) {
        this.discountEntity = discountEntity;
    }

    @XmlTransient
    public List<SectionEntity> getSectionEntityList() {
        return sectionEntityList;
    }

    public void setSectionEntityList(List<SectionEntity> sectionEntityList) {
        this.sectionEntityList = sectionEntityList;
    }

    @XmlTransient
    public List<EnrollEntity> getEnrollEntityList() {
        return enrollEntityList;
    }

    public void setEnrollEntityList(List<EnrollEntity> enrollEntityList) {
        this.enrollEntityList = enrollEntityList;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public TypeEntity getTypeId() {
        return typeId;
    }

    public void setTypeId(TypeEntity typeId) {
        this.typeId = typeId;
    }

    /*public List<GoalEntity> getGoals() {
        return goals;
    }*/

    /*public void setGoals(List<GoalEntity> goals) {
        this.goals = goals;
    }*/

    public List<CommentEntity> getComments() {
        return comments;
    }

    public void setComments(List<CommentEntity> comments) {
        this.comments = comments;
    }
}
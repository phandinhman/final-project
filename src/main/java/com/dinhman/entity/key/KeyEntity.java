package com.dinhman.entity.key;

import com.dinhman.entity.answer.AnswerEntity;
import com.dinhman.entity.quiz.QuizEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Dinh Man on 24/04/2016.
 */
@Entity
@Table(name = "correct")
public class KeyEntity {

    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "delete_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteDate;
    @JoinColumn(name = "answer_id", referencedColumnName = "id")
    @ManyToOne
    private AnswerEntity answerId;
    @JoinColumn(name = "quiz_id", referencedColumnName = "id")
    @ManyToOne
    private QuizEntity quizId;

    public KeyEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public AnswerEntity getAnswerId() {
        return answerId;
    }

    public void setAnswerId(AnswerEntity answerId) {
        this.answerId = answerId;
    }

    public QuizEntity getQuizId() {
        return quizId;
    }

    public void setQuizId(QuizEntity quizId) {
        this.quizId = quizId;
    }

}

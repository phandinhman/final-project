package com.dinhman.entity.role;

import com.dinhman.entity.user.UserEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Dinh Man on 06/04/2016.
 */
@Entity
@Table(name = "role")
public class RoleEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @ManyToMany(mappedBy = "role")
    private Set<UserEntity> userEntityList;

    public RoleEntity() {
    }

    public RoleEntity(long id) {
        this.id = id;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Set<UserEntity> getUserEntityList() {
        return userEntityList;
    }

    public void setUserEntityList(Set<UserEntity> userEntityList) {
        this.userEntityList = userEntityList;
    }


    @Override
    public String toString() {
        return "RoleEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
package com.dinhman.entity.quiz;

import com.dinhman.entity.answer.AnswerEntity;
import com.dinhman.entity.key.KeyEntity;
import com.dinhman.entity.section.SectionEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 24/04/2016.
 */
@Entity
@Table(name = "quiz")
public class QuizEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String question;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "delete_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteDate;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @JoinColumn(name = "section_id", referencedColumnName = "id")
    @ManyToOne
    private SectionEntity sectionId;

    @OneToMany(mappedBy = "quizId")
    private List<AnswerEntity> listAnswer;

    @OneToMany(mappedBy = "quizId")
    private List<KeyEntity> listKey;

    public QuizEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public SectionEntity getSectionId() {
        return sectionId;
    }

    public void setSectionId(SectionEntity sectionId) {
        this.sectionId = sectionId;
    }

    public List<AnswerEntity> getListAnswer() {
        return listAnswer;
    }

    public void setListAnswer(List<AnswerEntity> listAnswer) {
        this.listAnswer = listAnswer;
    }
}

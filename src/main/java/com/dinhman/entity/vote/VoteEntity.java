package com.dinhman.entity.vote;

import javax.persistence.*;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Entity
@Table(name = "vote")
public class VoteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String description;

    private int star;

    public VoteEntity() {
    }

    public VoteEntity(String description, int star) {
        this.description = description;
        this.star = star;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }
}

package com.dinhman.entity.track;

import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.lecture.LectureEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Dinh Man on 24/04/2016.
 */
@Entity
@Table(name = "track")
public class TrackEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @JoinColumn(name = "lecture_id", referencedColumnName = "id")
    @ManyToOne
    private LectureEntity lectureId;

    @JoinColumn(name = "enroll_id", referencedColumnName = "id")
    @ManyToOne
    private EnrollEntity enrollId;

    public TrackEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public LectureEntity getLectureId() {
        return lectureId;
    }

    public void setLectureId(LectureEntity lectureId) {
        this.lectureId = lectureId;
    }

    public EnrollEntity getEnrollId() {
        return enrollId;
    }

    public void setEnrollId(EnrollEntity enrollId) {
        this.enrollId = enrollId;
    }
}

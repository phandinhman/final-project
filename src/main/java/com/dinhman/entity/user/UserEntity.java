package com.dinhman.entity.user;

import com.dinhman.entity.comment.CommentEntity;
import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.role.RoleEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Dinh Man on 19/03/2016.
 */
@Entity
@Table(name = "user")
public class UserEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "name")
    private String name;

    @Column(name = "facebook_id")
    private String facebookId;

    @OneToMany(mappedBy = "userId")
    private List<CourseEntity> courseEntityList;

    @OneToMany(mappedBy = "userId")
    private List<CommentEntity> commentEntityList;

    @OneToMany(mappedBy = "userId")
    private List<EnrollEntity> enrollEntityList;

    @ManyToMany
    @JoinTable(name= "user_role", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<RoleEntity> role;

    private String avatar;

    public UserEntity() {
    }

    public UserEntity(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @XmlTransient
    public List<CourseEntity> getCourseEntityList() {
        return courseEntityList;
    }

    public void setCourseEntityList(List<CourseEntity> courseEntityList) {
        this.courseEntityList = courseEntityList;
    }

    @XmlTransient
    public List<CommentEntity> getCommentEntityList() {
        return commentEntityList;
    }

    public void setCommentEntityList(List<CommentEntity> commentEntityList) {
        this.commentEntityList = commentEntityList;
    }

    @XmlTransient
    public List<EnrollEntity> getEnrollEntityList() {
        return enrollEntityList;
    }

    public void setEnrollEntityList(List<EnrollEntity> enrollEntityList) {
        this.enrollEntityList = enrollEntityList;
    }

    public Set<RoleEntity> getRoles() {
        return role;
    }

    public void setRoles(Set<RoleEntity> role) {
        this.role = role;
    }


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
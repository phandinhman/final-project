package com.dinhman.controller.course;

import com.dinhman.dto.comment.CommentDto;
import com.dinhman.dto.course.CourseDto;
import com.dinhman.dto.lecture.LectureDto;
import com.dinhman.dto.track.TrackDto;
import com.dinhman.dto.type.TypeDto;
import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.exception.ValidateException;
import com.dinhman.service.comment.CommentService;
import com.dinhman.service.course.CourseService;
import com.dinhman.service.enroll.EnrollService;
import com.dinhman.service.lecture.LectureService;
import com.dinhman.service.section.SectionService;
import com.dinhman.service.track.TrackService;
import com.dinhman.service.type.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Dinh Man on 23/03/2016.
 */
@Controller
@RequestMapping(value = "/course")
public class CourseController {

    @Autowired
    private SectionService sectionService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private EnrollService enrollService;

    @Autowired
    private LectureService lectureService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private TrackService trackService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/{id}")
    public String getDetailCourse(@PathVariable("id") Long courseId, Model model, HttpSession session) {

        UserDto user = (UserDto) session.getAttribute("user");

        EnrollEntity enroll = null;

        if (user != null) {
            enroll = enrollService.findEnrollByUserIdAndCourseId(user.getId(), courseId);
        }

        if (user != null && enroll != null) {
            return "redirect:/course/overview/{id}";
        }

        if (courseId != null) {

            CourseDto course = courseService.findById(courseId);
            model.addAttribute("course", course);

            List<CourseDto> sameType = courseService.sameType(course.getType().getId());
            model.addAttribute("courseSameType", sameType);

            if (user == null) {
                model.addAttribute("user", new UserDto());
            }

            return "course/courseDetail";
        } else {
            throw new ValidateException("");
        }
    }

    @RequestMapping(value = "overview/{id}")
    public String overview(@PathVariable("id") long idCourse, HttpSession session, Model model) {

        UserDto user = (UserDto) session.getAttribute("user");

        if (user == null) {
            return "redirect:/course/{id}";
        }

        EnrollEntity enroll = enrollService.findEnrollByUserIdAndCourseId(user.getId(), idCourse);

        if (enroll == null) {
            return "redirect:/course/{id}";
        }

        CourseDto course = courseService.findById(idCourse, enroll.getId());
        LectureDto nextLecture = lectureService.nextLecture(course.getId(), enroll.getId());


        model.addAttribute("course", course);
        model.addAttribute("lecture", nextLecture);

        return "course/overview";
    }

    @RequestMapping(value = "learn/{id}/{idLecture}")
    public String learn(@PathVariable("id") Long courseId, @PathVariable("idLecture") Long idLecture, HttpSession session, Model model) {

        UserDto user = (UserDto) session.getAttribute("user");

        if (user == null) {
            return "redirect:/course/{id}";
        }

        EnrollEntity enroll = enrollService.findEnrollByUserIdAndCourseId(user.getId(), courseId);

        if (user == null || enroll == null) {
            return "redirect:/course/{id}";
        }

        CourseDto course = courseService.findById(courseId, enroll.getId());
        LectureDto lecture = lectureService.findById(idLecture);

        LectureDto nextLecture = lectureService.nextLecture(course.getId(), enroll.getId());

        model.addAttribute("nextLecture", nextLecture);

        if (lecture == null) {
            return "redirect:/course/overview/{id}";
        }

        TrackDto track = new TrackDto();
        track.setEnrollId(enroll.getId());
        track.setLectureId(lecture.getId());
        trackService.create(track);

        model.addAttribute("course", course);
        model.addAttribute("lecture", lecture);

        return "course/learn";
    }

    @RequestMapping(value = "api/allCourse")
    public
    @ResponseBody
    List<CourseDto> getAllCourse() {
        return courseService.getALLCourse();
    }

    @RequestMapping(value = "myCourses")
    public String myCourse(Model model, HttpSession session) {

        UserDto user = (UserDto) session.getAttribute("user");

        if (user != null) {
            List<CourseDto> courses = courseService.enrolled(user.getId());
            model.addAttribute("courses", courses);
            return "course/courseList";
        }

        return "course/courseList";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getTopCourse(Model model, HttpSession session) {

        UserDto user = (UserDto) session.getAttribute("user");

        List<TypeDto> types = typeService.getAllType();
        model.addAttribute("types", types);

        List<CourseDto> courses = courseService.getByPage(1);
        model.addAttribute("courses", courses);

        if (user == null) {
            model.addAttribute("user", new UserDto());
        }

        return "course/courseList";
    }

    @RequestMapping(value = "ajax")
    public String ajaxDemo() {
        return "course/ajax";
    }

    @RequestMapping(value = "api/comment/{courseId}/{page}", method = RequestMethod.GET)
    public
    @ResponseBody
    Object hi(@PathVariable Long courseId, @PathVariable Long page) {
        System.out.println(courseId + "xxx" + page);
        List<CommentDto> list = commentService.findComment(courseId, page.intValue());
        return list;
    }


    @RequestMapping(value = "search")
    public String searchCourse(@RequestParam("nameCourse") String name, Model model) {

        List<CourseDto> listCourse = courseService.findByName(name);

        List<TypeDto> listTypeDto = typeService.getAllType();

        model.addAttribute("listType", listTypeDto);

        if (listCourse != null && listCourse.size() > 0) {
            model.addAttribute("listCourse", listCourse);
        } else {
            model.addAttribute("message", "Your search \"" + name + "\" did not match any courses.");
        }
        return "course/courseList";
    }

}

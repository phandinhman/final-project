package com.dinhman.controller.google;

import com.dinhman.constant.Constants;
import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.user.UserEntity;
import com.dinhman.service.user.UserService;
import com.dinhman.utils.Google2Api;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.Mapper;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.*;
import org.scribe.oauth.OAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Dinh Man on 23/04/2016.
 */
@Controller
public class GoogleOAuth2Authenticator {

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Mapper mapper;

    private OAuthService oAuthService;

    public GoogleOAuth2Authenticator() {
        oAuthService = buildOAuthService();
    }

    private OAuthService buildOAuthService() {
        return new ServiceBuilder().provider(Google2Api.class)
                .apiKey(Constants.GOOGLE_CLIENT_ID).apiSecret(Constants.GOOGLE_CLIENT_SECRET).callback(Constants.GOOGLE_REDIRECT_URL)
                .scope(Constants.GOOGLE_SCOPE).build();
    }

    @RequestMapping("/auth/google")
    public RedirectView startAuthentication() {
        String authorizationUrl =
                oAuthService.getAuthorizationUrl(Token.empty());
        return new RedirectView(authorizationUrl);
    }

    @RequestMapping("/auth/google/callback")
    public String callback(@RequestParam("code") String code,
                           HttpSession session)
            throws IOException {
        Token accessToken = getAccessToken(code);

        Response response = getResponseForProfile(accessToken);

        UserDto user = userService.getUserFromGoogle(response);

        if (user != null) {
            session.setAttribute("user", user);
        } else {
            return "index";
        }

        return "redirect:/course/";
    }

    private Token getAccessToken(String code) {
        Verifier verifier = new Verifier(code);
        return oAuthService.getAccessToken(Token.empty(), verifier);
    }

    private Response getResponseForProfile(Token accessToken) {
        OAuthRequest oauthRequest =
                new OAuthRequest(Verb.GET, Constants.GOOGLE_PROTECTED_RESOURCE_URL);
        oAuthService.signRequest(accessToken, oauthRequest);
        return oauthRequest.send();
    }


}

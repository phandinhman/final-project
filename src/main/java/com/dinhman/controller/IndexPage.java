package com.dinhman.controller;

import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.user.UserEntity;
import com.dinhman.repository.user.UserRepository;
import com.dinhman.service.course.CourseService;
import com.dinhman.service.lecture.LectureService;
import com.dinhman.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Dinh Man on 05/03/2016.
 */
@Controller
public class IndexPage {

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @Autowired
    private LectureService lectureService;

    @Autowired
    private UserRepository repository;

    @RequestMapping(value = {"/", "index"})
    public String index(Model model) {
        model.addAttribute("user", new UserDto());
        model.addAttribute("allCourse", courseService.getByPage(1));
        model.addAttribute("topCourse", courseService.topCourse());
        model.addAttribute("topLatest", courseService.topLatest());
        model.addAttribute("numberOfCourse", courseService.countCourse());
        model.addAttribute("numberOfStudent", userService.numberOfStudent());
        model.addAttribute("numberOfLecture", lectureService.numberOfLecture());
        return "index";
    }

    @RequestMapping("/login")
    public String getLogin() {
        return "login";
    }
}

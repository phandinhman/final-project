package com.dinhman.controller.enroll;

import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.service.enroll.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by Dinh Man on 11/09/2016.
 */
@Controller
@RequestMapping(value = "enroll")
public class EnrollController {

    @Autowired
    private EnrollService enrollService;

    @RequestMapping(value = "/{courseId}")
    public String enroll(@PathVariable Integer courseId, HttpSession session) {
        UserDto user = (UserDto) session.getAttribute("user");
        if (user != null) {
            EnrollEntity enroll = enrollService.save(user.getId(), courseId);
            if (enroll != null) {
                return "redirect:/course/overview/{courseId}";
            } else {
                return "course/registerFail";
            }
        } else {
            return "redirect:/";
        }
    }
}

package com.dinhman.controller.facebook;

import com.dinhman.constant.Constants;
import com.dinhman.dto.user.UserDto;
import com.dinhman.entity.user.UserEntity;
import com.dinhman.service.user.UserService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.Mapper;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.*;
import org.scribe.oauth.OAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Dinh Man on 18/04/2016.
 */
@Controller
public class FacebookScribeAuthenticator {

    @Autowired
    private UserService userService;

    @Autowired
    private Mapper mapper;

    private OAuthService oAuthService;

    public FacebookScribeAuthenticator() {
        this.oAuthService = buildOAuthService();
        //this.objectMapper.registerModule(new AfterburnerModule());
    }

    private OAuthService buildOAuthService() {
        // The callback must match Site-Url in the Facebook app settings
        return new ServiceBuilder()
                .apiKey(Constants.FACEBOOK_CLIENT_ID)
                .apiSecret(Constants.FACEBOOK_CLIENT_SECRET)
                .callback(Constants.FACEBOOK_HOST + "/auth/facebook/callback")
                .provider(FacebookApi.class)
                .build();
    }

    @RequestMapping("/auth/facebook")
    public RedirectView startAuthentication(HttpSession session) {
        String state = UUID.randomUUID().toString();
        session.setAttribute(Constants.FACEBOOK_STATE, state);
        String authorizationUrl =
                oAuthService.getAuthorizationUrl(Token.empty())
                        + "&" + Constants.FACEBOOK_STATE + "=" + state;
        return new RedirectView(authorizationUrl);
    }

    @RequestMapping("/auth/facebook/callback")
    public String callback(@RequestParam("code") String code,
                           @RequestParam(Constants.FACEBOOK_STATE) String state,
                           HttpSession session)
            throws IOException {
        String stateFromSession = (String) session.getAttribute(Constants.FACEBOOK_STATE);
        session.removeAttribute(Constants.FACEBOOK_STATE);
        if (!state.equals(stateFromSession)) {
            return "index";
        }

        Token accessToken = getAccessToken(code);
        Response response = getResponseForProfile(accessToken);
        if (!response.isSuccessful()) {
            return "index";
        }

        UserDto user = userService.getUserFromFacebook(response);

        session.setAttribute("user", user);
        return "redirect:/course/";
    }

    private Token getAccessToken(String code) {
        Verifier verifier = new Verifier(code);
        return oAuthService.getAccessToken(Token.empty(), verifier);
    }

    private Response getResponseForProfile(Token accessToken) {
        OAuthRequest oauthRequest =
                new OAuthRequest(Verb.GET, Constants.FACEBOOK_URL);
        oAuthService.signRequest(accessToken, oauthRequest);
        return oauthRequest.send();
    }


}

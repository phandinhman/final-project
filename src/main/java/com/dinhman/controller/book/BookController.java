package com.dinhman.controller.book;


import com.dinhman.entity.book.BookEntity;
import com.dinhman.service.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dinh Man on 06/03/2016.
 */
@Controller
@RequestMapping(value = "book")
public class BookController {

    @Autowired
    private BookService service;

    @ModelAttribute(value = "books")
    public List<BookEntity> findAllBook() {
        return service.findAllBook();
    }

    @RequestMapping(value = "/")
    public
    @ResponseBody
    Object bookHomepage() {
        Map map = new HashMap<>();
        map.put("ss", new String("xxx"));
        return map;
    }

    @RequestMapping(value = "bookDetail/{bookId}")
    public String bookDetail(@PathVariable("bookId") long id, Model model) {
        BookEntity bookEntity = service.findById(id);
        model.addAttribute("bookEntity", bookEntity);
        return "xx";
    }

}

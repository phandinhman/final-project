-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: online_db
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` bigint(20) NOT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `quiz_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKto2httb0ape4w9nvnyqxwqg79` (`quiz_id`),
  CONSTRAINT `FKto2httb0ape4w9nvnyqxwqg79` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_date` varchar(255) DEFAULT NULL,
  `delete_date` varchar(255) DEFAULT NULL,
  `update_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `course_id` bigint(20) DEFAULT NULL,
  `lecture_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8kcum44fvpupyw6f5baccx25c` (`user_id`),
  KEY `FKdsub2q6m6519rpas8b075fr7m` (`course_id`),
  KEY `FKeutqb0lxb8sh9ck7wb9cbw6jg` (`lecture_id`),
  CONSTRAINT `FK8kcum44fvpupyw6f5baccx25c` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKdsub2q6m6519rpas8b075fr7m` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FKeutqb0lxb8sh9ck7wb9cbw6jg` FOREIGN KEY (`lecture_id`) REFERENCES `lecture` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,'xxx',NULL,NULL,NULL,1,8,NULL),(2,'yyy',NULL,NULL,NULL,1,8,NULL),(3,'333',NULL,NULL,NULL,3,8,NULL),(4,'444',NULL,NULL,NULL,1,8,NULL),(5,'555',NULL,NULL,NULL,5,8,NULL),(6,'666',NULL,NULL,NULL,6,8,NULL),(7,'777',NULL,NULL,NULL,7,8,NULL),(8,'888',NULL,NULL,NULL,8,8,NULL),(9,'999',NULL,NULL,NULL,9,8,NULL),(10,'100',NULL,NULL,NULL,10,8,NULL),(11,'110',NULL,NULL,NULL,11,8,NULL),(12,'120',NULL,NULL,NULL,12,8,NULL),(13,'130',NULL,NULL,NULL,13,8,NULL),(14,'140',NULL,NULL,NULL,1,8,NULL);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `lecture_id` bigint(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgiftldcfgbw45aq1qax3164nl` (`lecture_id`),
  CONSTRAINT `FKgiftldcfgbw45aq1qax3164nl` FOREIGN KEY (`lecture_id`) REFERENCES `lecture` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,'Test',NULL,NULL,NULL,2,NULL),(2,'Test 2',NULL,NULL,NULL,2,NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correct`
--

DROP TABLE IF EXISTS `correct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `correct` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `answer_id` bigint(20) DEFAULT NULL,
  `quiz_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK98pqq2ca0tw16g5jb7mj92ugn` (`answer_id`),
  KEY `FK7c3ydndgwxuondt9ad0txq5ga` (`quiz_id`),
  CONSTRAINT `FK7c3ydndgwxuondt9ad0txq5ga` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`),
  CONSTRAINT `FK98pqq2ca0tw16g5jb7mj92ugn` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correct`
--

LOCK TABLES `correct` WRITE;
/*!40000 ALTER TABLE `correct` DISABLE KEYS */;
/*!40000 ALTER TABLE `correct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `cost` bigint(20) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKo3767wbj6ow5axv38qej0gxo9` (`user_id`),
  KEY `FKn7b9627ufta57ke0yds02ailt` (`type_id`),
  CONSTRAINT `FKn7b9627ufta57ke0yds02ailt` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  CONSTRAINT `FKo3767wbj6ow5axv38qej0gxo9` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'2016-04-27 13:13:01','Beginning Java','Beginning Java',1,99,'java-lap-trinh.jpg',1),(2,'2016-04-27 13:13:03','Beginning Scala Programming','Beginning Scala Programming',1,0,'307686_66ad_5.jpg',1),(3,'2016-04-27 13:13:02','Java Persistence Hibernate and JPA Fundamentals','Java Persistence Hibernate and JPA Fundamentals',1,49,'242180_db25_46.jpg',1),(4,'2016-04-27 13:13:04','Learn and Understand AngularJS','Learn and Understand AngularJS',13,59,'289230_1056_16.jpg',1),(5,'2016-04-27 13:13:05','Learn Bootstrap Development By Building 10 Projects','Learn Bootstrap Development By Building 10 Projects',1,199,'427530_d0e0_2.jpg',1),(6,'2016-04-27 13:13:07','Learn Nodejs by building 10 projects','Learn Nodejs by building 10 projects',7,0,'481002_0155_2.jpg',1),(7,'2016-04-27 13:13:09','Android Studio Course. Build Apps. Android 6.0 Marshmallow','Android Studio Course. Build Apps. Android 6.0 Marshmallow',10,69,'298212_5847_3.jpg',1),(8,'2016-04-27 13:13:08','AngularJS for Beginners, Single-Page Applications Made Easy','AngularJS for Beginners, Single-Page Applications Made Easy',11,59,'332252_4a47.jpg',1),(9,'2016-05-01 07:19:00','Create Spring 4 RESTful Web Service - Step By Step','Create Spring 4 RESTful Web Service - Step By Step',12,0,'194490_5bc5_4.jpg',1),(10,NULL,'Build Responsive Real World Websites with HTML5 and CSS3','Build Responsive Real World Websites with HTML5 and CSS3',19,35,'437398_46c3_7.jpg',1),(11,NULL,'Build Websites from Scratch with HTML & CSS','Build Websites from Scratch with HTML & CSS',20,25,'80940_2664_8.jpg',1),(12,NULL,'REST Web Services using Java EE','REST Web Services using Java EE',21,20,'472510_668e.jpg',1),(13,NULL,'The Professional Ruby on Rails Developer','The Professional Ruby on Rails Developer',22,30,'383204_6866_7.jpg',1);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `percent` double DEFAULT NULL,
  `course_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqbdl06ar18j8mda2uvjjjopkr` (`course_id`),
  CONSTRAINT `FKqbdl06ar18j8mda2uvjjjopkr` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount`
--

LOCK TABLES `discount` WRITE;
/*!40000 ALTER TABLE `discount` DISABLE KEYS */;
INSERT INTO `discount` VALUES (1,'2016-04-01 00:00:00','2016-03-30 00:00:00',NULL,'Happy Work Day','2016-05-31 00:00:00',50,1),(2,'2016-04-04 00:00:00','2016-03-01 00:00:00','2016-03-25 00:00:00','Happy Tet','2016-05-04 00:00:00',80,1),(3,'2016-04-01 00:00:00','2016-03-30 00:00:00','2016-03-25 00:00:00',NULL,'2016-05-30 00:00:00',85,1),(4,'2016-04-01 00:00:00','2016-03-22 00:00:00',NULL,NULL,'2016-05-30 00:00:00',78,2);
/*!40000 ALTER TABLE `discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enroll`
--

DROP TABLE IF EXISTS `enroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enroll` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enroll_date` datetime DEFAULT NULL,
  `course_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKiv2pkft2ab8mqx9ffc4ex4q7e` (`course_id`),
  KEY `FKji8dvms4a1wdp39pdsmikqh6j` (`user_id`),
  CONSTRAINT `FKiv2pkft2ab8mqx9ffc4ex4q7e` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FKji8dvms4a1wdp39pdsmikqh6j` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enroll`
--

LOCK TABLES `enroll` WRITE;
/*!40000 ALTER TABLE `enroll` DISABLE KEYS */;
INSERT INTO `enroll` VALUES (1,NULL,9,1),(2,NULL,2,1),(5,NULL,9,6),(6,NULL,9,5),(7,NULL,8,1),(9,NULL,8,3),(10,NULL,2,9),(11,NULL,1,18),(12,NULL,2,18),(13,NULL,8,18),(14,NULL,9,18);
/*!40000 ALTER TABLE `enroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goal`
--

DROP TABLE IF EXISTS `goal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `course_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdjt17i7tpp8b7aqukxmk8kn8e` (`course_id`),
  CONSTRAINT `FKdjt17i7tpp8b7aqukxmk8kn8e` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goal`
--

LOCK TABLES `goal` WRITE;
/*!40000 ALTER TABLE `goal` DISABLE KEYS */;
/*!40000 ALTER TABLE `goal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (199),(199),(199),(199);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecture`
--

DROP TABLE IF EXISTS `lecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lecture` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `source_code` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `section_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK568elaju5okd8k0hukt18mtk7` (`section_id`),
  CONSTRAINT `FK568elaju5okd8k0hukt18mtk7` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecture`
--

LOCK TABLES `lecture` WRITE;
/*!40000 ALTER TABLE `lecture` DISABLE KEYS */;
INSERT INTO `lecture` VALUES (1,NULL,NULL,'1',99,'Course Intro',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_01.MP4',1),(2,NULL,NULL,'2',NULL,'Course Source Code',NULL,NULL,NULL,1),(3,NULL,NULL,'3',384,'Angular Basic Concepts',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_02.MP4',2),(4,NULL,NULL,'4',NULL,'Access the lesson examples live!',NULL,NULL,NULL,2),(5,NULL,NULL,'5',NULL,'Creating a New Module',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_03.MP4',2),(6,NULL,NULL,'6',NULL,'Basic Controller',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_04.MP4',2),(7,NULL,NULL,'7',NULL,'One Way Data Binding',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_05.MP4',2),(8,NULL,NULL,'8',NULL,'Two Way Data Binding',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_06.MP4',2),(9,NULL,NULL,'9',NULL,'Repeating Elements',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_07.MP4',2),(10,NULL,NULL,'In this lesson we showcase the project app.',NULL,'Intro to Expenses Tracker',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_08.MP4',3),(11,NULL,NULL,'In this lesson we explain the layout with Bootstrap 3.',NULL,'Bootstrap Layout',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_09.MP4',3),(12,NULL,NULL,'In this lesson we implement the expenses listing in our SPA.',NULL,'Listing Expenses',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_10.MP4',3),(13,NULL,NULL,'In this lesson we talk about filters in Angular.',NULL,'Filters',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_11.MP4',3),(14,NULL,NULL,'In this lesson we take a first look at the routing system we want to implement in our SPA.',NULL,'Intro to Routing',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_12.MP4',3),(15,NULL,NULL,'In this lesson we complete the implementation of the routes.',NULL,'Implementing Routes',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_13.MP4',3),(16,NULL,NULL,'In this lesson we add parameters to a route.',NULL,'Route Parameters',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_14.MP4',3),(17,NULL,NULL,'In this lesson we implement a service to handle our expenses data.',NULL,'Services',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_15.MP4',3),(18,NULL,NULL,'In this lesson we implement the ability to add new entries to our listing.',NULL,'Adding New Entries',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_16.MP4',3),(19,NULL,NULL,'In this lesson we fix the dates so that we can use the HTML5 date input type, we also cover accessing Angular via the console.',NULL,'Fixing Dates and Console Access',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_17.MP4',3),(20,NULL,NULL,'In this lesson we implement a simple way to generate unique ID\'s.',NULL,'Generating ID\'s',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_18.MP4',3),(21,NULL,NULL,'In this lesson we implement a method to get an entry by it\'s ID.',NULL,'Updating Entries - Getting by ID',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_19.MP4',3),(22,NULL,NULL,'In this lesson we implement how to update an entry.',NULL,'Updating Entries',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_20.MP4',3),(23,NULL,NULL,'In this lesson we implement some basic form validation.',NULL,'Form Validation',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_21.MP4',3),(24,NULL,NULL,'In this lesson we implement entry deletion.',NULL,'Deleting Entries',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_22.MP4',3),(25,NULL,NULL,'In this lesson we talk about custom directives in Angular.',NULL,'Custom Directives',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_23.MP4',3),(26,NULL,NULL,'In this lesson we simulate a cloud connection and implement loading entries from a server.',NULL,'Connecting to the Cloud - Load Entries',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_24.MP4',3),(27,NULL,NULL,'In this lesson we simulate a cloud connection and implement creating entries.',NULL,'Connecting to the Cloud - Create Entries',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_25.MP4',3),(28,NULL,NULL,'In this lesson we simulate a cloud connection and implement updating entries.',NULL,'Connecting to the Cloud - Update Entries',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_26.MP4',3),(29,NULL,NULL,'In this lesson we simulate a cloud connection and implement deleting entries.',NULL,'Connecting to the Cloud - Delete Entries',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_27.MP4',3),(30,NULL,NULL,'Congratulations! you\'ve completed the course.',NULL,'Conclusion',NULL,NULL,'AngularJS for Beginners, Single-Page Applications Made Easy - Udemy_28.MP4',4),(31,NULL,NULL,NULL,1,'Introduction to course',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy.MP4',5),(32,NULL,NULL,NULL,1,'Download and Setup Gradle',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy_2.MP4',5),(33,NULL,NULL,'This video explains the workspace setup procedure.',1,'Create Workspace',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy_3.MP4',5),(34,NULL,NULL,NULL,1,'A word of caution',NULL,NULL,NULL,6),(35,NULL,NULL,NULL,1,'What is this build script?',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy_4.MP4',6),(36,NULL,NULL,NULL,1,'Create Application Entry Class',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy_5.MP4',7),(37,NULL,NULL,NULL,1,'Create REST Controller',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy_6.MP4',7),(38,NULL,NULL,NULL,1,'Execute our application',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy_7.MP4',8),(39,NULL,NULL,NULL,1,'Recap',NULL,NULL,'Create Spring 4 RESTful Web Service - Step By Step - Udemy_8.MP4',9);
/*!40000 ALTER TABLE `lecture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `section_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmlx8xst0rbhivy6wu01xn9mb0` (`section_id`),
  CONSTRAINT `FKmlx8xst0rbhivy6wu01xn9mb0` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz`
--

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate`
--

DROP TABLE IF EXISTS `rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate` (
  `id` bigint(20) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `vote_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlg6gndexxajuxet9nk5kh8d4j` (`vote_id`),
  CONSTRAINT `FKlg6gndexxajuxet9nk5kh8d4j` FOREIGN KEY (`vote_id`) REFERENCES `vote` (`id`),
  CONSTRAINT `fk_rate_enroll` FOREIGN KEY (`id`) REFERENCES `enroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate`
--

LOCK TABLES `rate` WRITE;
/*!40000 ALTER TABLE `rate` DISABLE KEYS */;
INSERT INTO `rate` VALUES (1,'Hay lam.','2016-05-05 17:33:44',5),(2,'Lam hay','2016-05-05 17:33:44',5),(5,'Sao ma hay ghe the','2016-05-05 17:33:44',4),(6,'Qua dinh','2016-05-05 17:33:44',3),(7,'Không thể tin được.','2016-05-05 17:33:44',5),(9,'Sao lại có khóa học hay như thế nhỉ.','2016-05-05 17:33:44',4),(10,NULL,NULL,5);
/*!40000 ALTER TABLE `rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin','Admin'),(2,'Teacher','Teacher'),(3,'Student','Student');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_create` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `course_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKoy8uc0ftpivwopwf5ptwdtar0` (`course_id`),
  CONSTRAINT `FKoy8uc0ftpivwopwf5ptwdtar0` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,NULL,NULL,NULL,NULL,'Course Intro',8),(2,NULL,NULL,NULL,NULL,'Angular Basics',8),(3,NULL,NULL,NULL,NULL,'Expenses Tracker App',8),(4,NULL,NULL,NULL,NULL,'Conclusion',8),(5,NULL,NULL,NULL,NULL,'Prepare your workspace',9),(6,NULL,NULL,NULL,NULL,'Understand build script and more',9),(7,NULL,NULL,NULL,NULL,'Coding time',9),(8,NULL,NULL,NULL,NULL,'Result Time',9),(9,NULL,NULL,NULL,NULL,'A full recap - Optional',9);
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `track`
--

DROP TABLE IF EXISTS `track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `track` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `enroll_id` bigint(20) DEFAULT NULL,
  `lecture_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKih0ivtilqef49xu2ys7wcb3t0` (`enroll_id`),
  KEY `FK8hkcypp12cbrdoryb22vkl7v0` (`lecture_id`),
  CONSTRAINT `FK8hkcypp12cbrdoryb22vkl7v0` FOREIGN KEY (`lecture_id`) REFERENCES `lecture` (`id`),
  CONSTRAINT `FKih0ivtilqef49xu2ys7wcb3t0` FOREIGN KEY (`enroll_id`) REFERENCES `enroll` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `track`
--

LOCK TABLES `track` WRITE;
/*!40000 ALTER TABLE `track` DISABLE KEYS */;
INSERT INTO `track` VALUES (1,NULL,7,1),(192,'2016-07-30 09:17:04',13,1),(193,'2016-07-30 09:18:45',13,1),(194,'2016-07-30 09:19:01',13,2),(195,'2016-07-30 09:19:14',13,2),(196,'2016-07-30 09:19:19',13,5),(197,'2016-07-30 09:19:26',13,6),(198,'2016-07-30 09:23:07',13,3);
/*!40000 ALTER TABLE `track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,'Java Web','Java web'),(2,'Android','Android'),(3,'Front end','Front end'),(4,'Spring Framework','Spring Framework');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`),
  CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2016-04-27 12:51:19','0982181xxx@gmail.com',NULL,'Phan','Man','James Gosling','123456',3,'james-gosling.jpg'),(3,'2016-04-27 12:57:19','test@gmail.com',NULL,'Phan','Man','Phan Dinh Man','123456',3,NULL),(5,'2016-04-27 12:13:18',NULL,'1724069817831397',NULL,NULL,'Phan Dinh Man',NULL,3,NULL),(6,'2016-04-27 13:13:00','bao@gmail.com',NULL,'Huynh','Bao','Huynh Sy Manh Bao','123456',3,NULL),(7,'2016-04-27 13:15:00','khuynh@gmail.com',NULL,'Mai','Khuynh','Mai Anh Khuynh','123456',3,'598757_b22a_2.jpg'),(8,'2016-04-27 13:17:00','timbuchalka@gmail.com',NULL,'Buchalka','Tim Buchalka','Tim Buchalka','123456',2,NULL),(9,'2016-04-27 13:22:00','johnthompson@gmail.com',NULL,'Thompson','John','John Thompson','123456',2,NULL),(10,'2016-04-27 13:29:00','antoniopachon@gmail.com',NULL,'Pachón ','Pachón ','Antonio Pachón ','123456',2,'4727610_7b80.jpg'),(11,'2016-04-27 13:30:00','Pablo Farias Navarro ',NULL,'Navarro','Pablo Farias','Pablo Farias Navarro','123456',2,'201592_beaa_5.jpg'),(12,'2016-05-01 07:21:00','appLabshyd@gmail.com',NULL,'Hyd','AppLabs','AppLabs Hyd','123456',2,'3897040_3fd1_2.jpg'),(13,NULL,NULL,NULL,NULL,NULL,'Anthony Alicea',NULL,2,'2467626_f2e0.jpg'),(16,'2016-05-13 10:52:01','phandinhman@gmail.com',NULL,'Man','Phan','Phan Man',NULL,3,'https://lh6.googleusercontent.com/-WptE4_s6fOQ/AAAAAAAAAAI/AAAAAAAAAlw/260xOAqEbX4/photo.jpg'),(17,'2016-05-13 10:54:24',NULL,'1730816987156680',NULL,NULL,'Phan Dinh Man',NULL,3,NULL),(18,'2016-05-15 11:17:29',NULL,'103299750088496',NULL,NULL,'Man Phan',NULL,3,NULL),(19,NULL,NULL,NULL,NULL,NULL,'Jonas Schmedtmann ',NULL,2,'7799204_2091_5.jpg'),(20,NULL,NULL,NULL,NULL,NULL,'Code College',NULL,2,'13289904_5c6e_2.jpg'),(21,NULL,NULL,NULL,NULL,NULL,'Taylor Corey',NULL,2,'352582_e3a9_2.jpg'),(22,NULL,NULL,NULL,NULL,NULL,'Mashrur Hossain',NULL,2,'7355082_a6e1_3.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vote`
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `star` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote`
--

LOCK TABLES `vote` WRITE;
/*!40000 ALTER TABLE `vote` DISABLE KEYS */;
INSERT INTO `vote` VALUES (1,NULL,1),(2,NULL,2),(3,NULL,3),(4,NULL,4),(5,NULL,5);
/*!40000 ALTER TABLE `vote` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-11 17:18:37
